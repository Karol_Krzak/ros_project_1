#!/usr/bin/env python
#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from turtlesim.srv import *
from math import pow, atan2, sqrt
import numpy as np


class TurtleBot:

    def __init__(self):
        rospy.init_node('turtlebot_controller', anonymous=True)

        self.velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)

        self.pose_subscriber = rospy.Subscriber('/turtle1/pose',Pose, self.update_pose)

        rospy.wait_for_service('/turtle1/teleport_absolute')
        self.teleportation_service = rospy.ServiceProxy('/turtle1/teleport_absolute',TeleportAbsolute)

        rospy.wait_for_service('/turtle1/set_pen')
        self.color_service = rospy.ServiceProxy('/turtle1/set_pen',SetPen)

        self.pose = Pose()
        self.rate = rospy.Rate(10)

    def update_pose(self, data):
        self.pose = data
        self.pose.x = round(self.pose.x, 4)
        self.pose.y = round(self.pose.y, 4)

    def spiral(self):
        rotation = 30
        change = 2
        speed = 16
        while (not rospy.is_shutdown()) and (rotation>1):
            if(self.pose.x > 10.8 or self.pose.x < 0.2 or self.pose.y >10.8 or self.pose.y <0.2):
                self.stop()
                return

            vel_msg=Twist()
            vel_msg.angular.z = rotation
            vel_msg.linear.x = speed
            change /= 1.074
            rotation -= change
            self.velocity_publisher.publish(vel_msg)
            self.rate.sleep()
        self.stop();
        return

    def stop(self):
        vel_msg=Twist()
        vel_msg.linear.x = 0
        rotation = 0
        self.velocity_publisher.publish(vel_msg)
        self.rate.sleep()

if __name__ == '__main__':
    try:
        x = TurtleBot()
        print(sys.argv)
        x.color_service(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]),2,0)
        while 1:
            x.spiral()
            x.teleportation_service(5.544445,5.544445,0)
            x.color_service((254*np.random.rand()+1),(254*np.random.rand()+1),(254*np.random.rand()+1),2,0)
    except rospy.ROSInterruptException:
        pass
