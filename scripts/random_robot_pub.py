#!/usr/bin/env python
import rospy, math, time
import numpy as np # For random numbers
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint


#Initialize publisher
pub = rospy.Publisher('/arm_controller/command',JointTrajectory, queue_size=10)
rospy.init_node('publish_traj')

while rospy.get_rostime().to_sec() == 0.0:
        time.sleep(0.1)

c = JointTrajectory()
c.header.stamp = rospy.Time.now()
c.header.frame_id ='fixed'
c.joint_names.append('hip')
c.joint_names.append('shoulder')

rospy.loginfo(c.points)
time =rospy.Time.now()
i=0
r = rospy.Rate(1)
while not rospy.is_shutdown():
    c = JointTrajectory()
    c.header.stamp = time
    c.header.frame_id ='fixed'
    c.joint_names.append('hip')
    c.joint_names.append('shoulder')

    p = JointTrajectoryPoint()
    x1 = np.random.rand()*4
    x2 = np.random.rand()*4
    p.positions.append(x1)
    p.positions.append(x2)
    c.points.append(p)

    c.points[0].time_from_start = rospy.Time(i+1, 0)
    #c.points[i].time_from_start = rospy.Time(i+1, 0)
    i+=1
    
    try:
        pub.publish(c)
    except rospy.ROSException: rospy.loginfo('error')
    rospy.loginfo(c)
    r.sleep()
