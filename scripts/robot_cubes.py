#!/usr/bin/env python

import rospy, tf, random
import tf_conversions
from gazebo_msgs.srv import DeleteModel, SpawnModel
from geometry_msgs.msg import *
from gazebo_msgs.srv import GetModelState,GetWorldProperties
import roslib
import sys
import copy
from sensor_msgs.msg import JointState
import moveit_commander
import moveit_msgs.msg
import shape_msgs.msg as shape_msgs
import math

class Robot:
    def __init__(self):
        self.move_counter = 0
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group = moveit_commander.MoveGroupCommander("Arm")
        self.joint_control_pub = rospy.Publisher("/jaco/joint_control", JointState, queue_size=1)
        self.display_trajectory_publisher = rospy.Publisher(
        '/move_group/display_planned_path',
        moveit_msgs.msg.DisplayTrajectory, queue_size=10)
        self.rate = rospy.Rate(10)

    def recognize_enviroment(self):
        self.model_coordinates = rospy.ServiceProxy('/gazebo/get_model_state',GetModelState)
        self.world_state = rospy.ServiceProxy('/gazebo/get_world_properties',GetWorldProperties)
        self.num_of_cubes =len(self.world_state().model_names)-3
        self.cubes_coordinates =[]
        rospy.sleep(3.)
        for i in range(self.num_of_cubes):  #to be changed by subscribing to a specific topic
            self.cubes_coordinates.append(self.model_coordinates("cube"+str(i), "link"))
        self.bucket_coordinates = self.model_coordinates("bucket", "link")

        for i in range(self.num_of_cubes):
            p = geometry_msgs.msg.PoseStamped()
            p.header.frame_id = self.robot.get_planning_frame()
            p.pose.position.x = self.cubes_coordinates[i].pose.position.x
            p.pose.position.y = self.cubes_coordinates[i].pose.position.y
            p.pose.position.z = self.cubes_coordinates[i].pose.position.z
            p.pose.orientation = self.cubes_coordinates[i].pose.orientation
            self.scene.add_box("cube"+str(i), p, (0.05, 0.05, 0.05))
        print('=====cubes added as an obstacles')
        p = geometry_msgs.msg.PoseStamped()
        p.header.frame_id = self.robot.get_planning_frame()
        p.pose.position.x = self.bucket_coordinates.pose.position.x
        p.pose.position.y = self.bucket_coordinates.pose.position.y
        p.pose.position.z = self.bucket_coordinates.pose.position.z + 0.1
        p.pose.orientation = self.bucket_coordinates.pose.orientation
        self.scene.add_box("bucket", p, (0.2, 0.2, 0.2))
        print('=====bucket added as an obstacles')

    def move(self,x,y,z):

        self.group.get_planning_frame()
        self.group.get_end_effector_link()
        self.robot.get_group_names()
        self.robot.get_current_state()

        self.group.set_goal_orientation_tolerance(0.1)
        self.group.set_goal_tolerance(0.1)
        self.group.set_goal_joint_tolerance(0.1)
        self.group.set_num_planning_attempts(100)
        print(self.group)
        ## Planning to a Pose goal
        print ("============ Generating move: "+str(self.move_counter))
        self.move_counter += 1

        pose_goal = self.group.get_current_pose().pose
        waypoints = []

        #pose_goal.orientation = geometry_msgs.msg.Quaternion(*tf_conversions.transformations.quaternion_from_euler(0.,  0.  , 0.))
        waypoints.append(pose_goal)
        pose_goal.position.x = x
        pose_goal.position.y = y
        pose_goal.position.z = z
        pose_goal.orientation = geometry_msgs.msg.Quaternion(*tf_conversions.transformations.quaternion_from_euler(2.330175, -1.524318, 0.419453))
        #pose_goal.orientation = geometry_msgs.msg.Quaternion(*tf_conversions.transformations.quaternion_from_euler(0,0,0))

        #Create waypoints
        waypoints.append(pose_goal)

        #createcartesian  plan
        (plan1, fraction) = self.group.compute_cartesian_path(
                                            waypoints,   # waypoints to follow
                                            0.01,        # eef_step
                                            0.0)         # jump_threshold
        rospy.sleep(0.5)

        ## Moving to a pose goal
        print "============ Executing"
        self.group.execute(plan1,wait=True)
        rospy.sleep(1.)

    def gripper_open(self,state): #if state == true then open the gripper
        currentJointState = rospy.wait_for_message("/joint_states",JointState)
        currentJointState.header.stamp = rospy.get_rostime()
        if (state):
            tmp = 0.005
        else:
            tmp = 0.7
        currentJointState.position = tuple(list(currentJointState.position[:6]) + [tmp] + [tmp]+ [tmp])
        for i in range(3):
            self.joint_control_pub.publish(currentJointState)
            self.rate.sleep()
        rospy.sleep(1.)

class Object_spawner():
    def __init__(self):
        #rospy.wait_for_service("/jaco/joint_control")
        rospy.wait_for_service("gazebo/delete_model")
        rospy.wait_for_service("gazebo/spawn_sdf_model")
        print('====Object spawner: services registered')
        self.delete_model = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
        self.spawn_model = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
        self.orient = Quaternion(*tf_conversions.transformations.quaternion_from_euler(0., 0.0, 0.785398))
    def clean_enviroment(self):
        self.world_state = rospy.ServiceProxy('/gazebo/get_world_properties',GetWorldProperties)
        self.prev_num_of_cubes =len(self.world_state().model_names)-3
        for i in range(self.prev_num_of_cubes):
            self.delete_model("cube"+str(i))
        self.delete_model("bucket")
        print('=====enviroment cleaned')

    def spawn_cubes(self):
        with open("/home/maciek/catkin_ws/src/hello_ros/urdf/cube.urdf", "r") as f:
            product_xml = f.read()
        self.num_of_cubes = random.randint(1,6)
        for num in xrange(0,self.num_of_cubes):
            bin_x   =   random.uniform(0.1,0.5)
            bin_y   =   random.uniform(0.1,0.5)
            item_name   =   "cube{}".format(num)
            print("Spawning model:%s", item_name)
            item_pose   =   Pose(Point(x=bin_x, y=bin_y,    z=1),   self.orient)
            self.spawn_model(item_name, product_xml, "", item_pose, "world")
    def spawn_bucket(self):
        with open("/home/maciek/catkin_ws/src/hello_ros/urdf/bucket.urdf", "r") as f:
            product_xml = f.read()
        item_pose   =   Pose(Point(x=0.53, y=-0.23,    z=0.78),   self.orient)
        print("Spawning model:%s", "bucket")
        self.spawn_model("bucket", product_xml, "", item_pose, "world")

if __name__ == '__main__':
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("spawn_products_in_bins")
    object_spawner = Object_spawner()
    object_spawner.clean_enviroment()
    object_spawner.spawn_cubes()
    object_spawner.spawn_bucket()

    robot = Robot()
    robot.recognize_enviroment()
    robot.move(0,0,1.5)
    # robot.move(robot.bucket_coordinates.pose.position.x+0.02,robot.bucket_coordinates.pose.position.y+0.05,robot.bucket_coordinates.pose.position.z+0.5)
    robot.gripper_open(True)
    for i in range(robot.num_of_cubes):  #to be changed by subscribing to a specific topic
        robot.move(robot.cubes_coordinates[i].pose.position.x,robot.cubes_coordinates[i].pose.position.y,robot.cubes_coordinates[i].pose.position.z+0.307)
        robot.move(robot.cubes_coordinates[i].pose.position.x,robot.cubes_coordinates[i].pose.position.y,0.935) #0.92
        robot.gripper_open(False)
        robot.move(robot.cubes_coordinates[i].pose.position.x,robot.cubes_coordinates[i].pose.position.y,robot.cubes_coordinates[i].pose.position.z+0.307)
        robot.move(robot.bucket_coordinates.pose.position.x+0.02,robot.bucket_coordinates.pose.position.y+0.05,robot.bucket_coordinates.pose.position.z+0.5)
        robot.gripper_open(True)
