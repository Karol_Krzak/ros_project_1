#!/usr/bin/env python

import rospy, tf, random
import tf_conversions
from gazebo_msgs.srv import DeleteModel, SpawnModel
from geometry_msgs.msg import *
from gazebo_msgs.srv import GetModelState
import roslib
import sys
import copy
from sensor_msgs.msg import JointState
import moveit_commander
import moveit_msgs.msg
import shape_msgs.msg as shape_msgs
import math


def move_group_python_interface(x,y,z):
  ## BEGIN_TUTORIAL
  ## First initialize moveit_commander and rospy.
  print "============ Starting move setup"


  robot = moveit_commander.RobotCommander()
  scene = moveit_commander.PlanningSceneInterface()
  group = moveit_commander.MoveGroupCommander("Arm")



  print "============ Starting "
  ## We can get the name of the reference frame for this robot
  print "============ Reference frame: %s" % group.get_planning_frame()
  ## We can also print the name of the end-effector link for this group
  print "============ End effector frame: %s" % group.get_end_effector_link()
  ## We can get a list of all the groups in the robot
  print "============ Robot Groups:"
  print robot.get_group_names()
  ## Sometimes for debugging it is useful to print the entire state of the
  ## robot.
  print "============ Printing robot state"
  print robot.get_current_state()
  print "============"

  ## Let's setup the planner
  #group.set_planning_time(0.0)
  group.set_goal_orientation_tolerance(0.01)
  group.set_goal_tolerance(0.01)
  group.set_goal_joint_tolerance(0.01)
  group.set_num_planning_attempts(100)

  ## Planning to a Pose goal
  print "============ Generating plan 1"

  pose_goal = group.get_current_pose().pose
  waypoints = []

  #pose_goal.orientation = geometry_msgs.msg.Quaternion(*tf_conversions.transformations.quaternion_from_euler(0.,  0.  , 0.))
  waypoints.append(pose_goal)
  pose_goal.position.x = x
  pose_goal.position.y = y
  pose_goal.position.z = z
  pose_goal.orientation = geometry_msgs.msg.Quaternion(*tf_conversions.transformations.quaternion_from_euler(2.330175, -1.524318, 0.419453))
  print pose_goal

  #Create waypoints
  waypoints.append(pose_goal)

  #createcartesian  plan
  (plan1, fraction) = group.compute_cartesian_path(
                                      waypoints,   # waypoints to follow
                                      0.01,        # eef_step
                                      0.0)         # jump_threshold
  #plan1 = group.retime_trajectory(robot.get_current_state(), plan1, 1.0)


  print "============ Waiting while RVIZ displays plan1..."
  rospy.sleep(0.5)


  ## You can ask RVIZ to visualize a plan (aka trajectory) for you.
  print "============ Visualizing plan1"
  display_trajectory = moveit_msgs.msg.DisplayTrajectory()
  display_trajectory.trajectory_start = robot.get_current_state()
  display_trajectory.trajectory.append(plan1)
  display_trajectory_publisher.publish(display_trajectory);
  print "============ Waiting while plan1 is visualized (again)..."
  rospy.sleep(2.)

  ## Moving to a pose goal
  group.execute(plan1,wait=True)
  rospy.sleep(1.)

  ## END_TUTORIAL
  print "============ STOPPING"
  R = rospy.Rate(100)

def gripper_control(state): #if state == true then open the gripper
    currentJointState = rospy.wait_for_message("/joint_states",JointState)
    print 'Received!'
    currentJointState.header.stamp = rospy.get_rostime()
    if (state):
         tmp = 0.005
    else:
         tmp = 0.7
    currentJointState.position = tuple(list(currentJointState.position[:6]) + [tmp] + [tmp]+ [tmp])
    rate = rospy.Rate(100) # 10hz
    for i in range(3):
        joint_control_pub.publish(currentJointState)
        print 'Published!'
        rate.sleep()
    print 'end!'

currentJointState = JointState()
def jointStatesCallback(msg):
  global currentJointState
  currentJointState = msg


if __name__ == '__main__':
    print("Waiting for gazebo services...")
    rospy.init_node("spawn_products_in_bins")
    joint_control_pub = rospy.Publisher("/jaco/joint_control", JointState, queue_size=1)
    rospy.wait_for_service("gazebo/delete_model")
    rospy.wait_for_service("gazebo/spawn_sdf_model")
    print("Got it.")
    delete_model = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
    spawn_model = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)

    moveit_commander.roscpp_initialize(sys.argv)
    ## trajectories for RVIZ to visualize.
    display_trajectory_publisher = rospy.Publisher(
                                        '/move_group/display_planned_path',
                                        moveit_msgs.msg.DisplayTrajectory)

    with open("/home/maciek/catkin_ws/src/hello_ros/urdf/cube.urdf", "r") as f:
        product_xml = f.read()

    orient = Quaternion(*tf_conversions.transformations.quaternion_from_euler(0., 0.0, 0.785398))

    num_of_cubes = random.randint(2,6)

    for num in xrange(0,num_of_cubes):
        bin_y   =   random.uniform(0,0.5)
        bin_x   =   random.uniform(0,0.5)
        item_name   =   "cube{}".format(num)
        print("Spawning model:%s", item_name)
        item_pose   =   Pose(Point(x=bin_x, y=bin_y,    z=1),   orient)
        spawn_model(item_name, product_xml, "", item_pose, "world")

    with open("/home/maciek/catkin_ws/src/hello_ros/urdf/bucket.urdf", "r") as f:
        product_xml = f.read()

    item_pose   =   Pose(Point(x=0.53, y=-0.23,    z=0.78),   orient)
    print("Spawning model:%s", "bucket")
    spawn_model("bucket", product_xml, "", item_pose, "world")

    #second part

    model_coordinates = rospy.ServiceProxy('/gazebo/get_model_state',GetModelState)
    cubes_coordinates =[]
    rospy.sleep(3.)
    for i in range(num_of_cubes):  #to be changed by subscribing to a specific topic
        cubes_coordinates.append(model_coordinates("cube"+str(i), "link"))

    bucket_coordinates = model_coordinates("bucket", "link")

    move_group_python_interface(bucket_coordinates.pose.position.x+0.1,bucket_coordinates.pose.position.y,bucket_coordinates.pose.position.z+0.6)
    gripper_control(True)
    for i in range(num_of_cubes):  #to be changed by subscribing to a specific topic
        move_group_python_interface(cubes_coordinates[i].pose.position.x,cubes_coordinates[i].pose.position.y,cubes_coordinates[i].pose.position.z+0.3)
        move_group_python_interface(cubes_coordinates[i].pose.position.x,cubes_coordinates[i].pose.position.y,0.93)
        gripper_control(False)
        move_group_python_interface(cubes_coordinates[i].pose.position.x,cubes_coordinates[i].pose.position.y,cubes_coordinates[i].pose.position.z+0.3)
        move_group_python_interface(bucket_coordinates.pose.position.x+0.1,bucket_coordinates.pose.position.y,bucket_coordinates.pose.position.z+0.6)
        gripper_control(True)
