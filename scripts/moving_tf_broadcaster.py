#!/usr/bin/env python
import rospy
import tf
import math as math

if __name__ == '__main__':
    rospy.init_node('moving_tf_broadcaster')
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(10.0)
    time = rospy.Time.now().to_sec()
    print(time)
    while not rospy.is_shutdown():
        br.sendTransform((math.sin(rospy.Time.now().to_sec()),math.cos(rospy.Time.now().to_sec()),0.0),(0.0, 0.0, 0.0, 0.0),rospy.Time.now(),"carrot1","turtle1")
        rate.sleep()
